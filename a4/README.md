> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Advanced Mobile Web Application Development

## Jimmy Anderson

### Assignment 4 Requirements:

*Three Parts:*

1. Create Interest Paid on Loan Application
2. Chapter Questions (Chp. 11 - 12)
3. Java Skill Sets 10 - 12

#### README.md file should include the following items:

* Gif of Interest Paid on Loan Application
* Screenshots of Java Skill Sets 10 - 12

#### Assignment Screenshots:

| *Gif of Interest Paid On Loan Calculator *note: no invalid input, due to use of radio buttons for years and defaulted to 10 years* *: |
| :----:        |
| ![Interest Calculator](img/a4.gif) |

| *Screenshot of Java Skill Set Ten - Travel Time*: | *Screenshot of Java Skill Set Eleven - Product Class*: |
| :----:        |    :----:   |
| ![Java Skill Set Ten](img/ss10.png) | ![Java Skill Set Eleven](img/ss11.png) |

| *Screenshot of Java Skill Set Twelve - Book Inherits Product*: |
|   :----:    |
| ![Java Skill Set Twelve](img/ss12.png) |