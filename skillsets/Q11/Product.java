//Circle class calculates diameter, area, and circumference
public class Product
{
    //class variable: variables common to *all* objects, use static keyword
    //instance variable: each object has own set of instance variables
    private String fname;
    private String lname;
    private double age;

    //Java doesn't support parameters with default values (like C#, PHP, and C++)
    //default constructor
    public Product()
    {
        System.out.println("\nInside product default constructor.");
        fname = "abc123";
        lname = "My Widget";
        age = 49.99;
    }

    //parameterized constructor
    public Product(String fname, String lname, double age)
    {
        System.out.println("\nInside product constructor with parameters.");
        // must use this keyword when parameter names are same as field names
        this.fname = fname;
        this.lname = lname;
        this.age = age;
    }

    //getter/setter (accessor/mutator) methods
    public String getFname()
    {
        return fname;
    }

    public void setFname(String fn)
    {
        //set instance variable value to argument value
        fname = fn;
    }

    // Instance method calculates diameter
    public String getLname()
    {
        return lname;
    }

    //Instance method calculates area
    public void setLname(String ln)
    {
        //Area = PI * Radius-squared
        lname = ln;
    }

    //Instance method calculates circumference
    public double getAge()
    {
        //Circumference = 2 * PI * Radius
        return age;
    }

    public void setAge(double a)
    {
        //set instance variable value to parameter value
        age = a;
    }

    public void print()
    {
        System.out.println("\nCode: " + fname + ", Description: " + lname + ", Price: $" + age);
    }
}