import java.util.Scanner;
import java.text.DecimalFormat;

class ProductDemo
{
    public static void main(String[] args)
    {
        //initialize variables and create Scanner object
        String fn = "";
        String ln = "";
        double a = 0.0;
        Scanner sc = new Scanner(System.in);

        System.out.println("Developer: Jimmy Anderson");
        System.out.println("\n/////Below are default constructor values://///");
        Product p1 = new Product(); //create default object
        System.out.println("\nCode = " + p1.getFname());
        System.out.println("Description = " + p1.getLname());
        System.out.println("Price = $" + p1.getAge());

        System.out.println("\n/////Below are user-entered values://///");

        // get user input
        System.out.print("\nCode: ");
        fn = sc.nextLine();

        System.out.print("Description: ");
        ln = sc.nextLine();

        System.out.print("Price: ");
        a = sc.nextDouble();

        Product p2 = new Product(fn, ln, a);
        System.out.println("\nCode = " + p2.getFname());
        System.out.println("Description = " + p2.getLname());
        System.out.println("Price = $" + p2.getAge());

        System.out.println("\n/////Below using setter methods to pass literal values, then print() method to display values://///");
        p2.setFname("xyz789");
        p2.setLname("Test Widget");
        p2.setAge(89.99);
        p2.print();
        //System.out.println("Lname = " + p2.getLname());
    }
}