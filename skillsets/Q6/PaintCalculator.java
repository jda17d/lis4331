// A graphical equivalent of the classic "Hello world" program.
import java.text.DecimalFormat;

import javax.swing.*; // for GUI components

public class PaintCalculator {
    public static void main(String[] args) {
        JOptionPane.showMessageDialog(null, "Program uses Java GUI message and input dialogs.\nProgram determines paint cost per room(i.e., \"area\").\nFor paint \"area\" simplicity: use length X height X 2 + width X height X 2.\nFormat numbers as per below: thousand serparator, decimal point and $ sign for currency.\nResearch how many square feet a gallon of paint covers.\nNote: Program performs data validation.");
    
        String title = "Paint Cost Calculator";
        double cost = 0.0;
        double length = 0.0;
        double height = 0.0;
        double width = 0.0;
        double total = 0.0;
        double area = 0.0;
        boolean check = false;
        final double paint = 350.000032;

do{
  String numberText = JOptionPane.showInputDialog(null,"Paint price per gallon : ", title, JOptionPane.INFORMATION_MESSAGE);
  try {
    cost = Double.parseDouble(numberText);
    check = true;
  } catch(NumberFormatException e) {
    check = false;
  }
}
while(check == false);

do{
  String numberText2 = JOptionPane.showInputDialog(null,"Length : ", title, JOptionPane.INFORMATION_MESSAGE);
  try {
    length = Double.parseDouble(numberText2);
    check = true;
  } catch(NumberFormatException d) {
    check = false;
  }
}
while(check == false);

do{
  String numberText3 = JOptionPane.showInputDialog(null,"Width : ", title, JOptionPane.INFORMATION_MESSAGE);
 try {
    width = Double.parseDouble(numberText3);
    check = true;
  } catch(NumberFormatException b) {
    check = false;
  }
}
while(check == false);

do{
  String numberText4 = JOptionPane.showInputDialog(null,"Height : ", title, JOptionPane.INFORMATION_MESSAGE);
  try {
    height = Double.parseDouble(numberText4);
    check = true;
  } catch(NumberFormatException c) {
    check = false;
  }
}
while(check == false);

DecimalFormat df2 = new DecimalFormat( "#,###,###,##0.00" );

area = (length * height * 2) + (width * height * 2);
total = (area / paint) * cost;
String st1 = "Paint = $" + df2.format(cost) + " per gallon.\nArea of room = " + df2.format(area) + "sq ft.\nTotal = $" + df2.format(total) + "";
JOptionPane.showMessageDialog(null, st1);
}
}
