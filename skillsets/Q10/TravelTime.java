import java.util.Scanner;

public class TravelTime
{
    public static void main(String[] args)
    {
        /*


        nut



        */

        System.out.println("Program calculates approximate travel time");
        System.out.println(); // print blank line

        //initialize variables
        double miles = 0.0;
        double mph = 0.0;
        double hours = 0.0;
        boolean isValidMiles = false;
        boolean isValidMPH = false;

        //convert to int
        int minutes = 0;
        int hoursInt = 0;
        //String choice = "y"; //if doing a pretest while loop (to continue)
        String choice = ""; //if doing a posttest do...while loop (to continue)

        // create Scanner object
        Scanner sc = new Scanner(System.in);

        // perform calculations until choice isn't equal to "y" or "Y"
        do
        {
            //assume invalid data entry
            while (isValidMiles == false)
            {
                //check miles double
                System.out.print("Please enter miles: ");
                if (sc.hasNextDouble())
                {
                    miles = sc.nextDouble();
                    isValidMiles = true;
                }
                else
                {
                    System.out.println("Invalid double--miles must be a number.\n");
                }
                sc.nextLine(); // discard any other data entered on line

                // miles data true, check miles range
                if (isValidMiles == true && miles <= 0 || miles > 3000)
                {
                    System.out.println("Miles must be greater than 0, and no more than 3000.\n");
                    isValidMiles = false;
                }
                //check valid MPH and calculate
                else if (isValidMiles == true && isValidMPH == false)
                {
                    //check valid MPH
                    while (isValidMPH == false)
                    {
                        System.out.print("Please enter MPH: ");
                        if (sc.hasNextDouble())
                        {
                            mph = sc.nextDouble();
                            isValidMPH = true;
                        }
                        else
                        {
                            System.out.println("Invalid double--MPH must be a number.\n");
                            isValidMPH = false;
                        }
                        sc.nextLine(); //discard any other data entered on line
                        //check valid MPH range
                        if (isValidMPH == true && mph <= 0 || mph > 100)
                        {
                            System.out.println("MPH must be greater than 0, and no more than 100.\n");
                            isValidMPH = false;
                        }
                    }
                }
            if (isValidMiles == true && isValidMPH == true)
            {
                System.out.println();

                // calculate travel times in hours with decimal division
                hours = miles / mph;

                // get number of minutes as int
                minutes = (int) (hours * 60);

                // integer arithmetic to get hours and minutes as int values
                hoursInt = minutes / 60;
                minutes = minutes % 60;

                // display result
                System.out.println("Estimated travel time: " + hoursInt + " hr(s) " + minutes + " Minutes");

            }
        }

    //must reset test variables
    isValidMiles = false;
    isValidMPH = false;

    // continue?
    System.out.print("\nContinue? (y/n): ");
    choice = sc.next();
    System.out.println();
    }
    while (choice.equalsIgnoreCase("y"));
    }
}