import java.util.Scanner;

public class NestedStructures2
{
    public static void main(String[] args)
    {
        //print statements to show name program details
        System.out.println("Developer: Jimmy Anderson");
        System.out.println("Program counts, totals, and averages total number of user-entered exam scores.");
        System.out.println("Please enter exam scores between 0 and 100, inclusive.");
        System.out.println("Enter out of range number to end program.");
        System.out.println("Must *only* permit numeric entry.\n");

        double total = 0.0;
        int count = 0;
        double score = 0.0;
        double average = 0.0;
        Scanner input = new Scanner(System.in);

        //prompt user to take input
        while(score >= 0 && score <= 100)
        {
            System.out.print("Enter exam score: ");

            while(!input.hasNextDouble())
            {
                System.out.println("Not valid integer!\n");
                input.next();//IMPORTANT! if omittedm will go into infinite loop
                System.out.print("Please try again. Enter exam score: "); 
            }
        score = input.nextDouble();

        //count and total score
        if(score >= 0 && score <= 100)
            {
                //count = count++; // does not work because postfix ++ is applied after assignment
                count = ++count;
                total = total + score;
            }
        }
    average = total / count;

    //display values
    System.out.println("Count: " + count);
    System.out.println("Total: " + total);
    System.out.println("Average: " + average);
    }
}