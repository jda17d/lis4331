import java.util.Scanner;

public class MeasurementConversion
{
    public static void main (String [] args)
    {
        /*
            Measurement conversion:
            inch to meters = inch * 0.0254
        */

        System.out.println("Program converts inches to centimeters, meters, feet, yards, and miles.");
        System.out.println("***Notes***: \n1) Use integer for inches (must validate integer input). \n2) Use printf() function to print (format values per below output). \n3) Create Java \"constants\" for the following values: \n\tINCHES_TO_CENTIMETER, \n\tINCHES_TO_METER, \n\tINCHES_TO_FOOT, \n\tINCHES_TO_YARD, \n\tFEET_TO_MILE\n");

        int inches = 0;
        double centimeters = 0.0;
        double meters = 0.0;
        double feet = 0.0;
        double yards = 0.0;
        double miles = 0.0;

        //constants
        final double INCHES_TO_CENTIMETER = 2.54;
        final double INCHES_TO_METER = .0254;
        final double INCHES_TO_FOOT = 12;
        final double INCHES_TO_YARD = 36;
        final double FEET_TO_MILES = 5280;

        Scanner input = new Scanner(System.in);

        System.out.print("Please enter number of inches: ");
        while (!input.hasNextInt())
        {
            System.out.println("Not valid integer!\n");
            input.next();//Important! If omitted, will go into infinite loop on invalid input!
            System.out.print("Please enter number of inches: ");
        }
        inches = input.nextInt();

        centimeters = inches * INCHES_TO_CENTIMETER;
        meters = inches * INCHES_TO_METER;
        feet = inches / INCHES_TO_FOOT;
        yards = inches / INCHES_TO_YARD;
        miles = feet / FEET_TO_MILES;

        //System.out.println(inches + " inch(es) equals\n\n" + feet + " feet\n" + yards + " yard(s)\n" + centimeters + " centimeter(s)\n" + miles + " mile(s).");
        System.out.printf("%,d inch(es) equals\n\n%,.6f centimeter(s)\n%,.6f meter(s)\n%,.6f feet\n%,.6f yard(s)\n%,.8f mile(s)\n", inches, centimeters, meters, feet, yards, miles);
        //System.out.printf ("The product of %d and $d is $d" , product, num2, num1);

        /*
            Note: Using comma (,) flag to display numbers with thousands separator.

            Validating Input:
            http://stackoverflow.com/questions/3059333/validating-input-using-java-util-Scanner
            Note: how much easier Scanner.hasNextInt() is to use compared to the more verbose try/catch Integer.parseInt/NumberFormatException combo.
            By contract, a Scanner guarantees that if it hasNextInt(), then nextInt() will peacefully give you that int, and will not throw any exceptions

            Note that snuppet contains sc.next() statement to advance Scanner until it hasNextInt().
            It's important to realize that none of the hasNextXXX methods advance the Scanner past any input!
            You will find that if you omit sc.next() from the snippet, then it'd go into infinite loop on an invalid input!
            http://stackoverflow.com/questions/3059333/validating-input-using-java-util-Scanner
            */
    }
}