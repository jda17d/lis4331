//s
//s
//s

import java.io.*;

public class FileRead
{
    public static void main(String [] args)
    {
        //file to open
        String fileName = "filewrite.txt";

        //get one line at a time
        String line = null;

        try {
            //get JVM
            // CP1252
            //System.out.println(System.getProperty("file.encoding"));

            //FileReader reads text files in default encoding
            FileReader fileReader = new FileReader(fileName);

            //Note:
            //convert into
            //instead: wrap
            BufferedReader bufferedReader = new Bufferedreader(fileReader);

            //loop through lines in file
            while((line = bufferedReader.readLine()) != null)
            {
                System.out.println(line);
            }

        //close files
        bufferedReader.close();
        }

        catch(FileNoteFoundException ex)
        {
            System.out.println("Unable to open file " + fileName + "");
        }
        catch(IOException ex)
        {
            System.out.println("Error reading file " + fileName + "");

            //Or...print Error
            // ex.printStackTrace();
        }
    }
}