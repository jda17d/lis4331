import java.util.Scanner;

class Book extends Product
{
    //instance variable: each object has own set of instance variables
    private String author;

    //Java doesn't support parameters with default values (like C#, PHP, and C++)
    //default constructor
    public Book()
    {
        super();
        //System.out.println("\nInside product default constructor.");
        //super(); //will generate error!
        author = "John Doe";
    }

    //parameterized constructor
    public Book(String f, String l, double a, String s)
    {
        //Note: super() *must* be first element in constructor that calls constructor in its superclass.
        //Also, call super() only in constructor
        super(f,l,a);
        System.out.println("\nInside book constructor with parameters.");

        //note: didn't have to use "this" keyword, as parameters have different names than fields
        author = s;
    }

    //getter/setter (accessor/mutator) methods
    public String getAuthor()
    {
        return author;
    }

    public void setAuthor(String s)
    {
        author = s;
    }

    //subclass can override (replace) inherited method--that is, subclass's version of method is called instead

    //overidden method must have *same* signature (i.e., same name, parameter list, and return type)
    public void print()
    {
        super.print();
        System.out.println(", Author: " + author);
    }
}