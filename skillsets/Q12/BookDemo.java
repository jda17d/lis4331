
//Note:
//superclass = parent = base
//subclass = child = derived
import java.util.Scanner;

class BookDemo
{
    Scanner sc = new Scanner(System.in);
    public static void main(String[] args)
    {
        //initialize variables and create Scanner object
        String fn = "";
        String ln = "";
        double a = 0.0;
        String s = "";
        Scanner sc = new Scanner(System.in);

        System.out.println();
        System.out.println("Developer: Jimmy Anderson");
        System.out.println("\n/////Below are *base class default constructor* values (instantiating p1, then using getter methods)://///");
        Product p1 = new Product(); //create default object
        System.out.println("\nInside product default constructor.");
        System.out.println("\nCode = " + p1.getFname());
        System.out.println("Description = " + p1.getLname());
        System.out.println("Price = $" + p1.getPrice());

        System.out.println("\n/////Below are *base class* user-entered values (instantiating p2, then using getter methods)://///");

        // get user input
        System.out.print("\nCode: ");
        fn = sc.nextLine();

        System.out.print("Description: ");
        ln = sc.nextLine();

        System.out.print("Price: ");
        a = sc.nextDouble();

        Product p2 = new Product(fn, ln, a);
        System.out.println("\nCode = " + p2.getFname());
        System.out.println("Description = " + p2.getLname());
        System.out.println("Price = $" + p2.getPrice());

        System.out.println("\n/////Below using setter methods to pass literal values to p2, then print() method to display values://///");
        p2.setFname("xyz789");
        p2.setLname("Test Widget");
        p2.setPrice(89.99);
        p2.print();
        //System.out.println("Lname = " + p2.getLname());

        System.out.print("\n/////Below are *derived class default constructor* values (instantiating b1, then using getter methods)://///");
        System.out.println();
        System.out.print("\nInside product default constructor.");
        System.out.println();
        System.out.print("\nInside book default constructor.");
        System.out.println();
        Book e1 = new Book();
        System.out.println("\nCode = " + e1.getFname());
        System.out.println("Description = " + e1.getLname());
        System.out.println("Price = $" + e1.getPrice());
        System.out.println("Author = " + e1.getAuthor());
        System.out.println();
        System.out.println("Or using overridden derived class print() method...");
        e1.print();

        System.out.print("\n/////Below are *derived class* user-entered values (instantiating b2, then using getter methods)://///");

        System.out.println();
        Scanner sc2 = new Scanner(System.in);
        // get user input
        System.out.print("\nCode: ");
        fn = sc2.nextLine();

        System.out.print("Description: ");
        ln = sc2.nextLine();

        System.out.print("Price: ");
        a = sc2.nextDouble();

        Scanner sc3 = new Scanner(System.in);
        System.out.print("Author: ");
        s = sc3.nextLine();

        Book e2 = new Book(fn, ln, a, s);
        System.out.println("\nCode = " + e2.getFname());
        System.out.println("Description = " + e2.getLname());
        System.out.println("Price = $" + e2.getPrice());
        System.out.println("Author = " + e2.getAuthor());
        System.out.println();

        System.out.println("\nOr using derived class print() method...");
        System.out.println();
        e2.print();
    }
}