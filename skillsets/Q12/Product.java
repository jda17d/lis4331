import java.util.Scanner;
//Circle class calculates diameter, area, and circumference
public class Product
{
    //class variable: variables common to *all* objects, use static keyword
    //instance variable: each object has own set of instance variables
    private String fname;
    private String lname;
    private double price;

    //Java doesn't support parameters with default values (like C#, PHP, and C++)
    //default constructor
    public Product()
    {
        //System.out.println("\nInside product default constructor.");
        fname = "abc123";
        lname = "My Widget";
        price = 49.99;
    }

    //parameterized constructor
    public Product(String fname, String lname, double price)
    {
        System.out.println("\nInside product constructor with parameters.");
        // must use this keyword when parameter names are same as field names
        this.fname = fname;
        this.lname = lname;
        this.price = price;
    }

    //getter/setter (accessor/mutator) methods
    public String getFname()
    {
        return fname;
    }

    public void setFname(String fn)
    {
        //set instance variable value to argument value
        fname = fn;
    }

    // Instance method calculates diameter
    public String getLname()
    {
        return lname;
    }

    //Instance method calculates area
    public void setLname(String ln)
    {
        //Area = PI * Radius-squared
        lname = ln;
    }

    //Instance method calculates circumference
    public double getPrice()
    {
        //Circumference = 2 * PI * Radius
        return price;
    }

    public void setPrice(double a)
    {
        //set instance variable value to parameter value
        price = a;
    }

    public void print()
    {
        System.out.println("\nCode: " + fname + ", Description: " + lname + ", Price: $" + price);
    }
}