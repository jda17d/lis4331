// A graphical equivalent of the classic "Hello world" program.
import javax.swing.*; // for GUI components

public class EvenOrOdd {
    public static void main(String[] args) {
        JOptionPane.showMessageDialog(null, "Program uses Java GUI message and input dialogs.\nProgram evaluates integers as even or odd.\nNote: Program *does* perform data validation,\nprompting user until correct data entered.");
        String numberText = JOptionPane.showInputDialog(null,"Enter integer: ", "Number Test Dialog", JOptionPane.INFORMATION_MESSAGE);

  try {
    int number = Integer.parseInt(numberText);
  } catch(NumberFormatException e) {
    numberText = JOptionPane.showInputDialog(null,"Invalid integer. Please enter integer");
  }



        int number = Integer.parseInt(numberText);
        if(number % 2 == 0){
        JOptionPane.showMessageDialog(null, + (number) + " is an even number.");
        }
        else{
        JOptionPane.showMessageDialog(null, + (number) + " is an odd number.");
        }
        }
    }