import java.util.Scanner; //import statement to allow the program to take user input

public class MultipleNumber {
    public static void main(String[] args) {
        //print statements to show name program details
        System.out.println("Developer: Jimmy Anderson");
        System.out.println("Program determines if first number is multiple of second, prints result.");
        System.out.println("Example: 2, 4, 6, 8 are multiples of 2.");
        System.out.println("1) Use integers. 2) Use printf() function to print.");
        System.out.println("Must *only* permit integer entry.\n");

        int num1 = 0;
        int num2 = 0;
        int valid = 0;
        int product = 0;

        Scanner input = new Scanner(System.in); //create scanner object for user input

        //prompt user to take input
        System.out.print("Num1: ");
        while(!input.hasNextInt())
        {
            System.out.println("Not valid integer!\n");
            input.next();//IMPORTANT! if omittedm will go into infinite loop
            System.out.print("Please try again. Enter Num1: ");
        }

        num1 = input.nextInt();

        System.out.print("Num2: ");
        
        while(!input.hasNextInt())
        {
            System.out.println("Not valid integer!\n");
            input.next();//IMPORTANT! if omittedm will go into infinite loop
            System.out.print("Please try again. Enter Num2: ");    
        }
        num2 = input.nextInt();

        //valid multiple number
        valid = num1 % num2;

        System.out.println();

        if(valid == 0)
        {
            product = num1 / num2;
            System.out.printf("%d is a multiple of %d\n" , num1, num2);
            System.out.printf("The product of %d and %d is %d", product, num2, num1);
        }
        else
        System.out.printf("%d is not a multiple of %d", num1, num2);

        System.out.println();
    }
}