> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4331 - Advanced Mobile Web Application Development

## Jimmy Anderson

### Assignment 2 Requirements:

*Three Parts:*

1. Create Tip Calculator Application
2. Provide Screenshots of unpopulated and populated interface
3. Chapter Questions (Chp. 3)

#### README.md file should include the following items:

* Screenshot of Tip Calculator App: Unpopulated
* Screenshot of Tip Calculator App: Populated

#### Assignment Screenshots:

| *Screenshot of Tip Calculator App - Unpopulated Page*: | *Screenshot of Tip Calculator App - Populated Page*: |
| :----:        |    :----:   |
| ![Tip Calculator App Screenshot - Unpopulated](img/tipcalc1.png) | ![Tip Calculator App Screenshot - Populated](img/tipcalc2.png) |


| *Screenshot of Java Skill Set One*: | *Screenshot of Java Skill Set Two*:| *Screenshot of Java Skill Set Three*:|
| :----:        |    :----:   |    :----:   |
| ![Java Skill Set One](img/javaex1.png) | ![Java Skill Set Two](img/javaex2.png) | ![Java Skill Set Three](img/javaex3.png) |