> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4331 - Advanced Mobile Application Development

## Jimmy Anderson

### Project 2 Requirements:

*Four Parts:*

1. Created MyUsers Application making use of SQLite
2. Populated Database with 5 records
3. Provided Screenshots of Application
4. Chapter Questions (Introduction to Databases.PDF)

#### README.md file should include the following items:

* Gif of Running Application
* Screenshot of Splash Screen
* Screenshot of Add User Screen
* Screenshot of Update User Screen
* Screenshot of View User Screen
* Screenshot of Delete User Screen

#### Assignment Screenshots:

| *Gif of MyUsers Application*: |
| :----:        |
| ![P2](img/users.gif) |

| *Screenshot of Splash Page*: | *Screenshot of Add User Page*: |
| :----:        |    :----:   |
| ![Splash Page](img/splash.png) | ![Add User Page](img/add.png) |

| *Screenshot of Update User Page*: | *Screenshot of View DB Page*: | *Screenshot of Delete User Page*: |
| :----:        |    :----:   |    :----:   |
| ![Update User Page](img/update.png) | ![View DB Page](img/view.png) | ![Delete User Page](img/delete.png) |

#### Tutorial Links:

*P2 Helper:*
[P2 Helper](http://qcitr.com/vids/LIS4331_P2.mp4 "P2 Helper")