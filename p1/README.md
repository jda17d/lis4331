> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4331 - Advanced Mobile Web Application Development

## Jimmy Anderson

### Project 1 Requirements:

*Three Parts:*

1. Create Media Player Application
2. Complete Java Skill Sets ( 7 - 9 )
3. Chapter Questions ( Chp. 7 - 8 )

#### README.md file should include the following items:

* Screenshot of Media Player App: Splash Page
* Screenshot of Media Player App: Opening Page
* Screenshot of Media Player App: Playing
* Screenshot of Media Player App: Paused
* Screenshot of Java Skill Set 7
* Screenshot of Java Skill Set 8
* Screenshot of Java Skill Set 9

#### Assignment Screenshots:

| *Screenshot of Media Player App - Splash Page*: | *Media Player Application - Demo*: |
| :----:        |    :----:   |
| ![Media Player App Screenshot - Splash Page](img/splash.png) | ![Media Player Application - Demo](img/app.gif) |

| *Screenshot of Media Player App - Opening Page*: | *Screenshot of Media Player App - Playing*: | *Screenshot of Media Player App - Paused*: |
| :----:        |    :----:   |    :----:   |
| ![Media Player App Screenshot - Opening Page](img/open.png) | ![Media Player App Screenshot - Playing](img/playing.png) | ![Media Player App Screenshot - Paused](img/pause.png) |

| *Screenshot of Java Skill Set Seven - Measurement Conversion*: |
| :----:        |
| ![Java Skill Set One](img/ss7.png) |

| *Screenshot of Java Skill Set Eight - Pythagoras Theorem Calculator*: | *Screenshot of Java Skill Set Nine - Mutliple Selection List*: |
|   :----:   |   :----:    |
| ![Java Skill Set Two](img/ss8.png) | ![Java Skill Set Three](img/ss9.png) |