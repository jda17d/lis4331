> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4331 - Advanced Mobile Web Application Development

## Jimmy Anderson

### Assignment 5 Requirements:

*Four Parts:*

1. Create News Feed Application (RSSFeed)
2. Provide Screenshots of Application
3. Chapter Questions (Chp. 9 - 10)
4. Java Skill Set 13 - 15

#### README.md file should include the following items:

* Gif of News Feed Application
* Screenshots of Java Skill Sets

#### Assignment Screenshots:

| *Gif of News Reader Application*: |
| :----:        |
| ![News Reader App](img/NewsReader.gif) |

| *Screenshot of Java skill set Thirteen - Read / Write File*: |
| :----:        |
| ![Java Skill Set Thirteen](img/ss13.png) |

| *Screenshot of Java skill set Fourteen - Simple Interest Calculator*: | *Screenshot of skill set Fifteen - Array Demo*: |
|    :----:   | :----:        |
| ![Java Skill Set Fourteen](img/ss14.png) | ![Java Skill Set Fifteen](img/ss15.png) |

#### Tutorial Links:

*A5 Helper Video:*
[A5 Tutorial](http://qcitr.com/vids/LIS4331_A5.mp4 "A5 Tutorial")