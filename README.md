> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
# LIS4331 - Advanced Mobile Web Application Development

## Jimmy Anderson

### LIS4331 Requirements:

*Course Work Links:*

#### Assignments

1. [A1 README.md](a1/README.md "My A1 README.md file")
    * Installed JDK
    * Installed Android Studio and Create My First App
    * Provided screenshots of installations
    * Created Bitbucket repo
    * Completed Bitbucket tutorials (bitbucketstationlocations)
    * Provided git command descriptions
2. [A2 README.md](a2/README.md "My A2 README.md file")
    * Created Tip Calculator App
    * Provided Screenshot of the Application Unpopulated
    * Provided Screenshot of the Application Populated
    * Answered Chapter questions / Java skill sets
3. [A3 README.md](a3/README.md "My A3 README.md file")
    * Created Currency Converter App
    * Make use of Radio Buttons / Splash Screen / Toast Notification
    * Provided Screenshot of the Application Unpopulated
    * Provided Screenshot of the Application Toast Error
    * Provided Screenshot of the Application Populated
    * Answered Chapter questions / Java skill sets
4. [A4 README.md](a4/README.md "My A4 README.md file")
    * Created Interest Paid on Loan App
    * Provided Gif of Application Running
    * Provided Screenshots of Java Skill Sets
    * Answered Chapter Questions 
5. [A5 README.md](a5/README.md "My A5 README.md file")
    * Created News Feed RSS Application
    * Provided Gif of Application Running
    * Provided Screenshots of Java Skill Sets
    * Answered Chapter Questions

#### Projects

6. [P1 README.md](p1/README.md "My A6 README.md file")
    * Created Media Player Application
    * Included three bands/artists/songs
    * Made use of Android Media Player
    * Provided screenshots of working application
    * Provided screenshots of Java Skill Sets
    * Answered Chapter Questions
7. [P2 README.md](p2/README.md "My A7 README.md file")
    * Create My Users Application with SQLite
    * Populate Database with Users(5 min)
    * Included Splash Screen
    * Provided Screenshots of the Application Running
    * Answered Chapter Questions