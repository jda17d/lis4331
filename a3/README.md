> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4331 - Advanced Mobile Web Application Development

## Jimmy Anderson

### Assignment 3 Requirements:

*Four Parts:*

1. Create Android application to convert currency
2. Use radio buttons / Create splash screen / Use toast notifications 
3. Chapter Questions (Ch. 4 - 6)
4. Java Skill set (4 - 6)

#### README.md file should include the following items:

* Screenshot of Currency Conversion Application: Unpopulated
* Screenshot of Currency Conversion Application: Toast Invalid Input
* Screenshot of Currency Conversion Application: Populated
* Screenshots of Java Skill Sets (4 - 6)

#### Assignment Screenshots:

| *Screenshot of Currency Converter - Splash page*: | *Screenshot of Currency Converter - Unpopulated*: |
| :----:        |    :----:   |
| ![Currency Converter App Screenshot - Splash Page](img/splash.png) | ![Currency Converter App Screenshot - Unpopulated](img/converter1.png) |

| *Screenshot of Currency Converter - Toast Error*: | *Screenshot of Currency Converter - Populated*: |
| :----:        |    :----:   |
| ![Currency Converter App Screenshot - Toast Error](img/converter2.png) | ![Currency Converter App Screenshot - Populated](img/converter3b.png) |


| *Screenshot of Java skill set Four - Time Conversion*: | *Screenshot of Java skill set Five - Even or Odd*: |
| :----:        |    :----:   |
| ![Java Skill Set Four](img/ss4.png) | ![Java Skill Set Five](img/ss5.png) |

| *Screenshot of Java skill set Six - Paint Calculator*: |
|   :----:    |
| ![Java Skill Set Six](img/ss6.gif) |

#### Document Links:

*LIS4331 Repo Main:*
[LIS4331 Repo Main](https://bitbucket.org/jda17d/lis4331/src/ "LIS4331 Repo Main")