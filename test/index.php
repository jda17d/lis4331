<!DOCTYPE HTLM>
<html>
<head>
    <meta charset="utf-8">
    <title>Using RSS Feeds</title>
    <?php include_once("../css/include_css.php"); ?>
    </head>
<body>

<?php include_once("../global/nav.php"); ?>
    <?php
    //Note: RSS specification: https://validator.w3.org/feed/docs/rss2.html

    $html = "";
    $publisher = "BBC News U.S. & Canada RSS feed";
    $url = "http://feeds.bbci.co.uk/news/world/us_and_canada/rss.xml";

    $html .= '<h2>' . $publisher . '</h2>';
    $html .= $url;

    $rss = simplexml_load_file($url);
    $count = 0;
    $html .= '<ol>';
    foreach($rss->channel->item as $item)
    {
        $count++;
        if($count > 10)
        {
        break;
        }
        $html .= '<li><a href="'.htmlspecialchars($item->link).'">'. htmlspecialchars($item->title) . '</a><br />';
        $html .= htmlspecialchars($item->description) . '<br />';
        $html .= htmlspecialchars($item->pubDate) . '</li><br />';
    }
    $html .= '</ol>';

    print $html;
    ?>
    </body>
    </html>